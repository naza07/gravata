package com.example.exemploviewpager;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static android.widget.Toast.LENGTH_LONG;

public class MainActivity extends ListActivity {
	private int[] gravata = {R.drawable.cruzeiro1, R.drawable.cruzeiro2, R.drawable.gravata1,
							R.drawable.casariosecular, R.drawable.matriz1};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<Componentes> componentes = new ArrayList<Componentes>();

        //for(int i = 0; i < 5; i++){

            Componentes c1 = new Componentes();
            c1.setModelo("Gastronomia");
            c1.setMarca("teste");

            componentes.add(c1);

            Componentes c2 = new Componentes();
            c2.setModelo("Pontos Túristicos");
            c2.setMarca("teste");

            componentes.add(c2);

            Componentes c3 = new Componentes();
            c3.setModelo("Pontos Túristicos");
            c3.setMarca("teste");

            componentes.add(c3);


            Componentes c4 = new Componentes();
            c4.setModelo("Pontos Túristicos");
            c4.setMarca("teste");

            componentes.add(c4);

            Componentes c5 = new Componentes();
            c5.setModelo("Pontos Túristicos");
            c5.setMarca("teste");

            componentes.add(c5);

            Componentes c6 = new Componentes();
            c6.setModelo("Pontos Túristicos");
            c6.setMarca("teste");

            componentes.add(c6);
        //}

        final ListView lv = (ListView) findViewById(R.id.lv);

        lv.setAdapter(new ComponentesAdapter(this, componentes));
       // lv.setOnItemClickListener((AdapterView.OnItemClickListener) this);




        ViewPager vp = (ViewPager) findViewById(R.id.viewPager);
        vp.setAdapter(new AdapterImg(this, gravata));






    }
   /* @Override
    public void onListItemClick(ListView l, View v, int position, long id){
        Intent intent;
        switch(position){
            case 0:
                Log.e("eae","1");
                break;
            default: finish();
        }
    }*/




    }






